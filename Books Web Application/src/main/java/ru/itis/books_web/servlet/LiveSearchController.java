package ru.itis.books_web.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.books_web.models.Book;
import ru.itis.books_web.services.BooksService;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "LiveSearchController", value = "/live_search_controller")
public class LiveSearchController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String search = request.getParameter("query");
        BooksService booksService = (BooksService) getServletContext().getAttribute("book_service");
        List<Book> bookList = booksService.findByTitle(search);

        for (Book book : bookList) {
            response.getWriter().println("\n" +
                    "    <tr>\n" +
                    "        <td>" + book.getId() + "</td>\n" +
                    "        <td>" + book.getTitle() + "</td>\n" +
                    "        <td>" + book.getCountBook() + "</td>\n" +
                    "        <td>" + book.getAuthor() + "</td>\n" +
                    "        <td>" + book.getPages() + "</td>\n" +
                    "        <td>" + book.getPublicationDate() + "</td>\n" +
                    "    </tr>");
        }
    }
}
