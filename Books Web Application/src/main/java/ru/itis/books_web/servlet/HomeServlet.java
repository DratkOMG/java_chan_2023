package ru.itis.books_web.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HomeServlet", value = "/home_servlet")
public class HomeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/home.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();

        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Add book</title>\n" +
                "</head>\n" +
                "<body>");

        html.append("<form action=\"${pageContext.request.contextPath}/add_book_servlet\" method=\"get\">\n" +
                "    <input type=\"submit\" value=\"Add Book\">\n" +
                "</form>\n" +
                "\n" +
                "<form action=\"${pageContext.request.contextPath}/find_book_servlet\" method=\"get\">\n" +
                "    <input type=\"submit\" value=\"Find Book\">\n" +
                "</form>");

        html.append("</body>\n" +
                "</html>");

        writer.println(html);
    }
}
