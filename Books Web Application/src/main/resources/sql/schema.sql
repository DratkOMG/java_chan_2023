drop table if exists book;

create table Book
(
    id               bigserial primary key,
    title            varchar(50),
    count_book       integer,
    author           varchar(30),
    pages            integer default null,
    publication_date integer default null check (publication_date < 2023)
)