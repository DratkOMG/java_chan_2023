<%--
  Created by IntelliJ IDEA.
  User: DratkOMG
  Date: 10/29/2022
  Time: 20:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add book</title>
</head>
<body>
<h1>
    Welcome to DatsBook
</h1>

<form action="${pageContext.request.contextPath}/add_book_servlet" method="post">
    <input type="text" name="title" placeholder="title">
    <input type="text" name="author" placeholder="author">

    <p>${list}</p>

    <input type="submit" value="Add">
    <a href="${pageContext.request.contextPath}/home_servlet">
        Back
    </a>
</form>

</body>
</html>
