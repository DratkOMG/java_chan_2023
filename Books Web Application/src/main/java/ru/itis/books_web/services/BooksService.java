package ru.itis.books_web.services;

import ru.itis.books_web.models.Book;

import java.util.List;

public interface BooksService {

    void save(String title, String author);

    List<Book> viewBooksList();

    void deleteBookById(Long id);

    List<Book> findByTitle(String title);
    List<Book> findByAuthor(String author);
    List<Book> findByCountBookAndMark(int count, String mark);
    List<Book> findByPagesAndMark(int pages, String mark);
    List<Book> findByPublicationDateAndMark(int publicationDate, String mark);
}
