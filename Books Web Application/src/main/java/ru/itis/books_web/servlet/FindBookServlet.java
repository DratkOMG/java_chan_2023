package ru.itis.books_web.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.books_web.models.Book;
import ru.itis.books_web.services.BooksService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "FindBookServlet", value = "/find_book_servlet")
public class FindBookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/find-book.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter printWriter = response.getWriter();

        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String countBook = request.getParameter("count_book");
        String pages = request.getParameter("pages");
        String publicationDate = request.getParameter("publication_date");
        String mark = request.getParameter("mark");

        BooksService booksService = (BooksService) getServletContext().getAttribute("book_service");

        StringBuilder html = new StringBuilder();
        List<Book> bookList = new ArrayList<>();

        createHtml(title, author, countBook, pages, publicationDate, mark, booksService, html, bookList);

        printWriter.println(html);
    }

    private static void createHtml(String title, String author, String countBook, String pages, String publicationDate, String mark, BooksService booksService, StringBuilder html, List<Book> bookList) {
        html.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Test</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<table>\n" +
                "    <tr>\n" +
                "        <th>Title</th>\n" +
                "        <th>Author</th>\n" +
                "        <th>Count Book</th>\n" +
                "    </tr>\n");

        bookList = getBookList(title, author, countBook, pages, publicationDate, mark, booksService, bookList);


        for (Book b : bookList) {
            html.append("<tr>");
            html.append("<td>");
            html.append(b.getTitle());
            html.append("</td>");
            html.append("<td>");
            html.append(b.getAuthor());
            html.append("</td>");
            html.append("<td>");
            html.append(b.getCountBook());
            html.append("</td>");
            html.append("</tr>");
        }

        html.append("</table>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
    }

    private static List<Book> getBookList(String title, String author, String countBook, String pages, String publicationDate, String mark, BooksService booksService, List<Book> bookList) {
        if (title != null) {
            if (!(title.isBlank() || title.isEmpty())) {
                bookList = booksService.findByTitle(title);
            }
        }
        if (author != null) {
            if (!(author.isBlank() || author.isEmpty())) {
                bookList = booksService.findByAuthor(author);
            }
        }
        if (countBook != null) {
            if (!(countBook.isBlank() || countBook.isEmpty())) {
                bookList = booksService.findByCountBookAndMark(Integer.parseInt(countBook), mark);
            }
        }
        if (pages != null) {
            if (!(pages.isEmpty() || pages.isBlank())) {
                bookList = booksService.findByPagesAndMark(Integer.parseInt(pages), mark);
            }
        }
        if (publicationDate != null) {
            if (!(publicationDate.isBlank() || publicationDate.isEmpty())) {
                bookList = booksService.findByPublicationDateAndMark(Integer.parseInt(publicationDate), mark);
            }
        }

        return bookList;
    }
}
