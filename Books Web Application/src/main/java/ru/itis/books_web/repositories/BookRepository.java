package ru.itis.books_web.repositories;

import ru.itis.books_web.models.Book;

import java.util.List;
import java.util.Optional;

public interface BookRepository {
    Optional<Book> findById(Long id);

    void update(Integer countBook, Long id);

    void delete(Long id);

    List<Book> findAll();

    void save(Book book);
    void addBook(Book book);

//    something
    List<Book> findAllByDataAndByCountBook(int data, long countBook);

    boolean findByTitleAndByAuthor(Book book);
    List<Book> findByTitle(String title);
    List<Book> findByAuthor(String author);
    List<Book> findByCountBookAndMark(int count, String mark);
    List<Book> findByPagesAndMark(int count, String mark);
    List<Book> findByPublicationDateAndMark(int count, String mark);

}
