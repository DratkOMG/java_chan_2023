function liveSearch(param) {
    let search = param.value;

    $.ajax({
        url: "live_search_controller",
        type: "post",
        data: {
            query: search
        },
        success: function (response) {
            let row = document.getElementById("content");
            row.innerHTML = "<tr>\n" +
                "        <th>Id</th>\n" +
                "        <th>Title</th>\n" +
                "        <th>countBook</th>\n" +
                "        <th>author</th>\n" +
                "        <th>pages</th>\n" +
                "        <th>publicationDate</th>\n" +
                "    </tr>" +  response;
        }
    })
}