insert into book(title, count_book, author, pages, publication_date)
VALUES ('The Alchemist', 2, 'Paulo Coelho', 163, 1988);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('How to Win Friends and Influence People', 5, 'Dale Carnegie', 291, 1936);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('Think and Grow Rich', 10, 'Napoleon Hill', 238, 1937);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('How to Stop Worrying and Start Living', 7, 'Dale Carnegie', 306, 1948);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('The Godfather', 4, 'Mario Puzo', 448, 1969);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('The Magic of Thinking Big', 21, 'David J. Schwartz', 228, 1959);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('The One Thing', 23, 'Gary W. KellerJay Papasan', 240, 2013);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('A New Earth: Awakening to Your Life’s Purpose', 30, 'Eckhart Tolle', 336, 2005);

insert into book(title, count_book, author, pages, publication_date)
VALUES ('The 7 Habits of Highly Effective People', 19, 'Stephen R. Covey', 381, 1989);

insert into book(title, count_book, author)
VALUES ('Life''s Greatest Lessons', 4, 'Hal Urban');

insert into book(title, count_book, author, publication_date)
VALUES ('You Can Read Anyone', 25, 'David J. Lieberman', 2006);

insert into book(title, count_book, author, publication_date)
VALUES ('The Power of Attitude Hardcover', 4, 'Mac Anderson', 2004);
-- update book
-- set count_book = 12
-- where id = 4;
