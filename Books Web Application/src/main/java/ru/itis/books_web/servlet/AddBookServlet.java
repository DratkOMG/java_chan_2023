package ru.itis.books_web.servlet;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import ru.itis.books_web.models.Book;
import ru.itis.books_web.services.BooksService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "AddBookServlet", value = "/add_book_servlet")
public class AddBookServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/add-book.html").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        String title = request.getParameter("title");
        String author = request.getParameter("author");

        BooksService booksService = (BooksService) getServletContext().getAttribute("book_service");
        booksService.save(title, author);

        List<Book> bookList = booksService.viewBooksList();

        PrintWriter printWriter = response.getWriter();

        StringBuilder html = new StringBuilder();

        html.append("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Test</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<table>\n" +
                "    <tr>\n" +
                "        <th>Title</th>\n" +
                "        <th>Author</th>\n" +
                "        <th>Count Book</th>\n" +
                "    </tr>\n");

        for (Book b : bookList) {
            html.append("<tr>");
            html.append("<td>");
            html.append(b.getTitle());
            html.append("</td>");
            html.append("<td>");
            html.append(b.getAuthor());
            html.append("</td>");
            html.append("<td>");
            html.append(b.getCountBook());
            html.append("</td>");
            html.append("</tr>");
        }

        html.append("</table>\n" +
                "\n" +
                "</body>\n" +
                "</html>");

        printWriter.println(html);
    }
}
