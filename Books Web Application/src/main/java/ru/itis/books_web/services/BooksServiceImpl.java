package ru.itis.books_web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis.books_web.models.Book;
import ru.itis.books_web.repositories.BookRepository;

import java.util.List;

@Component
public class BooksServiceImpl implements BooksService {

    private final BookRepository bookRepository;

    @Autowired
    public BooksServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public void save(String title, String author) {
        Book book = Book.builder()
                .title(title)
                .author(author)
                .countBook(1)
                .build();

        if (title.isEmpty() || author.isEmpty() || author.isBlank() || title.isBlank()) {
            return;
        }

        if (bookRepository.findByTitleAndByAuthor(book)) {
            bookRepository.addBook(book);
        } else {
            bookRepository.save(book);
        }

    }

    @Override
    public List<Book> viewBooksList() {
        return bookRepository.findAll();
    }

    @Override
    public void deleteBookById(Long id) {
        bookRepository.delete(id);
    }

    @Override
    public List<Book> findByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    @Override
    public List<Book> findByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    @Override
    public List<Book> findByCountBookAndMark(int count, String mark) {
        return bookRepository.findByCountBookAndMark(count, mark);
    }

    @Override
    public List<Book> findByPagesAndMark(int pages, String mark) {
        return bookRepository.findByPagesAndMark(pages, mark);
    }

    @Override
    public List<Book> findByPublicationDateAndMark(int publicationDate, String mark) {
        return bookRepository.findByPublicationDateAndMark(publicationDate, mark);
    }
}
