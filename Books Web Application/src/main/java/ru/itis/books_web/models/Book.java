package ru.itis.books_web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder

public class Book {
    private Long id;
    private String title;
    private Integer countBook;
    private String author;
    private Integer pages;
    private Integer publicationDate;
}
