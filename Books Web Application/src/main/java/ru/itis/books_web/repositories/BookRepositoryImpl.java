package ru.itis.books_web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.itis.books_web.models.Book;

import javax.sql.DataSource;
import java.util.*;

@Component
public class BookRepositoryImpl implements BookRepository {

    //language=SQL
    private final String SQL_SELECT_BY_ID = "select * from book where id = :id";

    //language=SQL
    private final String SQL_UPDATE_BY_ID = "update book set count_book = :countBook where id = :id;";

    //language=SQL
    private final String SQL_DELETE_BY_ID = "delete from book where id = :id";

    //language=SQL
    private final String SQL_UPDATE_BY_TITLE_AND_BY_AUTHOR = "update book set count_book = count_book + 1 where title = :title and author = :author";
    //language=SQL
    private final String SQL_SELECT_ALL_BOOKS = "select * from book";

    //language=SQL
    private final String SQL_SELECT_BY_TITLE_AND_BY_AUTHOR = "select * from book where title = :title and author = :author";

    //language=SQL
    private final String SQL_SELECT_BY_DATA_AND_BY_COUNT_BOOK = "select * from book where publication_date > :publicationData and count_book > :countBook;";

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final RowMapper<Book> bookMapper = ((rs, rowNum) -> Book.builder()
            .id(rs.getLong("id"))
            .title(rs.getString("title"))
            .countBook(rs.getInt("count_book"))
            .author(rs.getString("author"))
            .pages(rs.getInt("pages"))
            .publicationDate(rs.getInt("publication_date"))
            .build());

    @Autowired
    public BookRepositoryImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Optional<Book> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    bookMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Integer countBook, Long id) {
        Map<String, Object> paramsUpdate = new HashMap<>();

        paramsUpdate.put("countBook", countBook);
        paramsUpdate.put("id", id);

        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, paramsUpdate);
    }

    @Override
    public void delete(Long id) {
        Map<String, Object> paramsDelete = new HashMap<>();

        paramsDelete.put("id", id);
        namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, paramsDelete);
    }

    @Override
    public List<Book> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BOOKS, bookMapper);
    }

    @Override
    public void save(Book book) {
        Map<String, Object> paramsSave = new HashMap<>();

        paramsSave.put("title", book.getTitle());
        paramsSave.put("count_book", book.getCountBook());
        paramsSave.put("author", book.getAuthor());
        paramsSave.put("pages", book.getPages());
        paramsSave.put("publication_date", book.getPublicationDate());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("book")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsSave)).longValue();

        book.setId(id);
    }

    @Override
    public void addBook(Book book) {
        Map<String, Object> params = new HashMap<>();

        params.put("title", book.getTitle());
        params.put("author", book.getAuthor());

        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_TITLE_AND_BY_AUTHOR, params);
    }

    @Override
    public List<Book> findAllByDataAndByCountBook(int publicationData, long countBook) {
        Map<String, Object> paramsFind = new HashMap<>();

        paramsFind.put("publicationData", publicationData);
        paramsFind.put("countBook", countBook);

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_DATA_AND_BY_COUNT_BOOK
                , paramsFind
                , bookMapper);
    }

    @Override
    public boolean findByTitleAndByAuthor(Book book) {
        Map<String, Object> params = new HashMap<>();

        params.put("title", book.getTitle());
        params.put("author", book.getAuthor());

        try {
            namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_TITLE_AND_BY_AUTHOR, params, bookMapper);
            return true;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }

    }

    @Override
    public List<Book> findByTitle(String title) {
        String sql = "select * from book where title ilike :title";

        return namedParameterJdbcTemplate.query(sql, Collections.singletonMap("title", "%" + title + "%"), bookMapper);

    }

    @Override
    public List<Book> findByAuthor(String author) {
        String sql = "select * from book where author = :author";

        return namedParameterJdbcTemplate.query(sql, Collections.singletonMap("author", author), bookMapper);
    }

    @Override
    public List<Book> findByCountBookAndMark(int countBook, String mark) {
        String sql = "select * from book where count_book" + mark + " :countBook";

        return namedParameterJdbcTemplate.query(sql, Collections.singletonMap("countBook", countBook), bookMapper);
    }

    @Override
    public List<Book> findByPagesAndMark(int pages, String mark) {
        String sql = "select * from book where pages" + mark + " :pages";

        return namedParameterJdbcTemplate.query(sql, Collections.singletonMap("pages", pages), bookMapper);
    }

    @Override
    public List<Book> findByPublicationDateAndMark(int publicationDate, String mark) {
        String sql = "select * from book where publication_date" + mark + " :publicationDate";

        return namedParameterJdbcTemplate.query(sql, Collections.singletonMap("publicationDate", publicationDate), bookMapper);
    }


}
