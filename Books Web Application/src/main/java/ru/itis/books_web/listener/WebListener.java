package ru.itis.books_web.listener;

import com.zaxxer.hikari.HikariDataSource;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis.books_web.config.ApplicationConfig;
import ru.itis.books_web.repositories.BookRepository;
import ru.itis.books_web.repositories.BookRepositoryImpl;
import ru.itis.books_web.services.BooksService;
import ru.itis.books_web.services.BooksServiceImpl;

import javax.sql.DataSource;

@jakarta.servlet.annotation.WebListener
public class WebListener implements ServletContextListener, HttpSessionListener, HttpSessionAttributeListener {
    private HikariDataSource dataSource;
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        /* This method is called when the servlet context is initialized(when the Web application is deployed). */
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        BooksService booksService = context.getBean(BooksService.class);
        this.dataSource = context.getBean(HikariDataSource.class);

        sce.getServletContext().setAttribute("book_service", booksService);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        this.dataSource.close();
    }
}
